import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import router from './router'
import { encrypt, decrypt } from './helpers/cryptoHelper.js'


Vue.use(Vuex, axios, router);

import regionesJSON from "./plugins/comunas.json";

export default new Vuex.Store({
	state: {
		serverRoute: 'http://localhost:3000',
		infoRegions: {
			regiones: [],
			comunas: []
		},
		regiones: regionesJSON.regions,
		responsePostForm: null


	},

	mutations: {

		getRegions(state) {
			for (let i = 0; i < state.regiones.length; i++) {
				let list = [];
				state.infoRegions.regiones.push({
					name: state.regiones[i].name,
					number: i
				});

				for (let j = 0; j < state.regiones[i].communes.length; j++) {
					list.push(state.regiones[i].communes[j]);
				}
				state.infoRegions.comunas.push(list)
			}
		},

		postService(state, data) {
      const encryptedData = encrypt(data)
			axios.post(state.serverRoute + '/form/create', encryptedData)
				.then(response => {

					const decryptedData = decrypt(response.data.encryptedData)

					state.responsePostForm = decryptedData
				})
				.catch(error => {
					// eslint-disable-next-line no-console
					console.log(error)
				})
		},
		/*
		getService(state)
		{
				axios.get(state.serverRoute + '/ruta')
						.then(response => {
								console.log(response.data)
								state.var = response.data
						})
						.catch(error => {
								console.log(error)
						})
		},

		 */
	}


})
